package org.thepoet.mr.service;

import org.thepoet.mr.model.Plateau;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public class PlateauService {

    private MarsRoverCommonsUtil commonsUtil;

    public PlateauService(MarsRoverCommonsUtil commonsUtil) {
        this.commonsUtil = commonsUtil;
    }

    public Plateau createPlateauFromInput(String coordinates) {
        String[] coordinatesArray = coordinates.split(" ");
        int upperX = commonsUtil.getIntegerValue(coordinatesArray[0]);
        int upperY = commonsUtil.getIntegerValue(coordinatesArray[1]);
        return new Plateau(upperX, upperY);
    }
}