package org.thepoet.mr.service.factory;

import org.thepoet.mr.enums.Command;
import org.thepoet.mr.enums.ErrorCode;
import org.thepoet.mr.exception.MarsRoverException;
import org.thepoet.mr.service.impl.MoveService;
import org.thepoet.mr.service.impl.TurnLeftService;
import org.thepoet.mr.service.impl.TurnRightService;
import org.thepoet.mr.service.spec.CommandService;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public class CommandServiceFactory {
    public CommandService get(Command command) {
        switch (command) {
            case L:
                return new TurnLeftService();
            case R:
                return new TurnRightService();
            case M:
                return new MoveService();
            default:
                throw new MarsRoverException(ErrorCode.NO_IMPLEMENTATION_OF_COMMAND, command.name());
        }
    }
}