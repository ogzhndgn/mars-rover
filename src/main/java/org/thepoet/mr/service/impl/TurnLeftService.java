package org.thepoet.mr.service.impl;

import org.thepoet.mr.enums.Direction;
import org.thepoet.mr.model.Rover;
import org.thepoet.mr.service.spec.CommandService;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public class TurnLeftService implements CommandService {
    @Override
    public void doCommand(Rover rover) {
        switch (rover.getDirection()) {
            case N:
                rover.setDirection(Direction.W);
                break;
            case W:
                rover.setDirection(Direction.S);
                break;
            case S:
                rover.setDirection(Direction.E);
                break;
            case E:
                rover.setDirection(Direction.N);
                break;
        }
    }
}