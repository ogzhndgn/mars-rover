package org.thepoet.mr.service.impl;

import org.thepoet.mr.enums.ErrorCode;
import org.thepoet.mr.exception.MarsRoverException;
import org.thepoet.mr.model.Location;
import org.thepoet.mr.model.Rover;
import org.thepoet.mr.service.spec.CommandService;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public class MoveService implements CommandService {
    @Override
    public void doCommand(Rover rover) {
        Location location = rover.getLocation();
        Location newLocation;
        switch (rover.getDirection()) {
            case N:
                newLocation = new Location(location.getCoordinateX(), location.getCoordinateY() + 1);
                break;
            case E:
                newLocation = new Location(location.getCoordinateX() + 1, location.getCoordinateY());
                break;
            case S:
                newLocation = new Location(location.getCoordinateX(), location.getCoordinateY() - 1);
                break;
            case W:
                newLocation = new Location(location.getCoordinateX() - 1, location.getCoordinateY());
                break;
            default:
                throw new MarsRoverException(ErrorCode.INVALID_DIRECTION, rover.getDirection().name());
        }
        if (!rover.getPlateau().isInPlateau(newLocation)) {
            throw new MarsRoverException(ErrorCode.OUT_OF_PLATEAU, newLocation.toString());
        }
        if (rover.getPlateau().isOccupiedPoint(newLocation)) {
            throw new MarsRoverException(ErrorCode.POINT_IS_OCCUPIED, newLocation.toString());
        }
        rover.setLocation(newLocation);
    }
}