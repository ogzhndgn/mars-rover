package org.thepoet.mr.service;

import org.thepoet.mr.enums.ErrorCode;
import org.thepoet.mr.exception.MarsRoverException;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public class MarsRoverCommonsUtil {

    public int getIntegerValue(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MarsRoverException(ErrorCode.INVALID_INPUT, s);
        }
    }
}