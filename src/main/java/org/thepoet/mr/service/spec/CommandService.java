package org.thepoet.mr.service.spec;

import org.thepoet.mr.model.Rover;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public interface CommandService {
    void doCommand(Rover rover);
}