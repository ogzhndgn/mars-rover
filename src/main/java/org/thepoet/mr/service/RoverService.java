package org.thepoet.mr.service;

import org.thepoet.mr.enums.Command;
import org.thepoet.mr.enums.Direction;
import org.thepoet.mr.enums.ErrorCode;
import org.thepoet.mr.exception.MarsRoverException;
import org.thepoet.mr.model.Location;
import org.thepoet.mr.model.Plateau;
import org.thepoet.mr.model.Rover;
import org.thepoet.mr.service.factory.CommandServiceFactory;
import org.thepoet.mr.service.spec.CommandService;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public class RoverService {

    private MarsRoverCommonsUtil commonsUtil;
    private CommandServiceFactory factory;

    public RoverService(MarsRoverCommonsUtil commonsUtil, CommandServiceFactory factory) {
        this.commonsUtil = commonsUtil;
        this.factory = factory;
    }

    public Rover createRoverFromInputAndLocate(String input, Plateau plateau) {
        String[] inputArray = input.split(" ");
        Location location = getLocation(inputArray);
        checkLocationValidity(plateau, location);
        Direction direction = Direction.toEnum(inputArray[2]);
        return new Rover(location, direction, plateau);
    }

    public void doCommands(Rover rover, String commands) {
        CommandService service;
        for (int i = 0; i < commands.length(); i++) {
            Command command = getCommand(commands, i);
            service = factory.get(command);
            service.doCommand(rover);
        }
    }

    private Command getCommand(String commands, int i) {
        String s = String.valueOf(commands.charAt(i));
        return Command.toEnum(s);
    }

    private Location getLocation(String[] inputArray) {
        int x = commonsUtil.getIntegerValue(inputArray[0]);
        int y = commonsUtil.getIntegerValue(inputArray[1]);
        return new Location(x, y);
    }

    private void checkLocationValidity(Plateau plateau, Location location) {
        if (!plateau.isInPlateau(location)) {
            throw new MarsRoverException(ErrorCode.OUT_OF_PLATEAU);
        }
        if (plateau.isOccupiedPoint(location)) {
            throw new MarsRoverException(ErrorCode.POINT_IS_OCCUPIED);
        }
    }
}