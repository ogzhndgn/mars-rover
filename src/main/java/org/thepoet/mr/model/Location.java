package org.thepoet.mr.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
@NoArgsConstructor
@Getter
@Setter
public class Location {
    private int coordinateX;
    private int coordinateY;

    public Location(int coordinateX, int coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public boolean isEqual(Location location) {
        return this.coordinateX == location.getCoordinateX() && this.coordinateY == location.getCoordinateY();
    }

    @Override
    public String toString() {
        return "X:" + coordinateX + " Y: " + coordinateY;
    }
}