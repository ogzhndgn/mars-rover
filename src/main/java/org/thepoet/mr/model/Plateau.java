package org.thepoet.mr.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */

@Getter
@Setter
@NoArgsConstructor
public class Plateau {
    private int upperX;
    private int upperY;
    private List<Rover> roverList;

    public Plateau(int upperX, int upperY) {
        this.upperX = upperX;
        this.upperY = upperY;
        this.roverList = new ArrayList<>();
    }

    public void addRoverToList(Rover rover) {
        this.roverList.add(rover);
    }

    public boolean isInPlateau(Location location) {
        if (location.getCoordinateX() < 0 || location.getCoordinateX() > this.upperX) {
            return false;
        }
        if (location.getCoordinateY() < 0 || location.getCoordinateY() > this.upperY) {
            return false;
        }
        return true;
    }

    public boolean isOccupiedPoint(Location location) {
        for (Rover roverInPlateau : this.roverList) {
            if (roverInPlateau.getLocation().isEqual(location)) {
                return true;
            }
        }
        return false;
    }
}