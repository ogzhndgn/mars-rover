package org.thepoet.mr.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.thepoet.mr.enums.Direction;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Rover {
    private Location location;
    private Direction direction;
    private Plateau plateau;

    @Override
    public String toString() {
        return this.location.getCoordinateX() + " " + this.location.getCoordinateY() + " " + this.direction;
    }
}