package org.thepoet.mr.exception;

import org.thepoet.mr.enums.ErrorCode;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public class MarsRoverException extends RuntimeException {
    public MarsRoverException(ErrorCode errorCode) {
        super(errorCode.name());
    }

    public MarsRoverException(ErrorCode errorCode, String detail) {
        super(errorCode.name() + " " + detail);
    }
}