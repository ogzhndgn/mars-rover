package org.thepoet.mr.enums;

import org.thepoet.mr.exception.MarsRoverException;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public enum Command {
    L,//LEFT
    R,//RIGHT
    M;//MOVE

    public static Command toEnum(String s) {
        try {
            return Command.valueOf(s);
        } catch (Exception e) {
            throw new MarsRoverException(ErrorCode.INVALID_COMMAND, s);
        }
    }
}