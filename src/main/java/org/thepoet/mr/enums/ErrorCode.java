package org.thepoet.mr.enums;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public enum ErrorCode {
    INVALID_COMMAND,
    INVALID_INPUT,
    INVALID_DIRECTION,
    OUT_OF_PLATEAU,
    POINT_IS_OCCUPIED,
    NO_IMPLEMENTATION_OF_COMMAND
}