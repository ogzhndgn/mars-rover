package org.thepoet.mr.enums;

import org.thepoet.mr.exception.MarsRoverException;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public enum Direction {
    N,//North
    E,//East
    S,//South
    W;//West

    public static Direction toEnum(String s) {
        try {
            return Direction.valueOf(s);
        } catch (Exception e) {
            throw new MarsRoverException(ErrorCode.INVALID_DIRECTION, s);
        }
    }
}
