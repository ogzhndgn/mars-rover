package org.thepoet.mr;

import org.thepoet.mr.model.Plateau;
import org.thepoet.mr.model.Rover;
import org.thepoet.mr.service.MarsRoverCommonsUtil;
import org.thepoet.mr.service.PlateauService;
import org.thepoet.mr.service.RoverService;
import org.thepoet.mr.service.factory.CommandServiceFactory;

import java.util.Scanner;

/**
 * @author Oguzhan Dogan <dogan_oguzhan@hotmail.com>
 */
public class MarsRoverRunner {
    public static void main(String[] args) {

        MarsRoverCommonsUtil commonsUtil = new MarsRoverCommonsUtil();
        CommandServiceFactory factory = new CommandServiceFactory();
        PlateauService plateauService = new PlateauService(commonsUtil);
        RoverService roverService = new RoverService(commonsUtil, factory);

        System.out.println("Hello From Mars Rover Application");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please Enter Plateau Upper Right Coordinates with Space (5 5): ");
        String coordinates = scanner.nextLine();

        Plateau plateau = plateauService.createPlateauFromInput(coordinates);
        String roverInformation;
        String commands;
        try {
            while (true) {
                System.out.print("Please Enter Rover Location and Direction (1 1 W): ");
                roverInformation = scanner.nextLine();
                Rover rover = roverService.createRoverFromInputAndLocate(roverInformation, plateau);
                plateau.addRoverToList(rover);
                System.out.print("Please Enter Rover Commands (LMRLRMLRMLMM): ");
                commands = scanner.nextLine();
                roverService.doCommands(rover, commands);
                System.out.println(plateau.getRoverList().size() + " " + rover.toString());
                for (Rover rover1 : plateau.getRoverList()) {
                    System.out.println("P: " + rover1.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            scanner.close();
        }
    }
}