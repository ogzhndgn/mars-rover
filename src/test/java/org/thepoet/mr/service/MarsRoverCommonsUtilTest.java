package org.thepoet.mr.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.thepoet.mr.exception.MarsRoverException;

@RunWith(MockitoJUnitRunner.class)
public class MarsRoverCommonsUtilTest {

    @InjectMocks
    MarsRoverCommonsUtil marsRoverCommonsUtil;

    @Test
    public void shouldGetIntegerValue() {
        int expected = 5;
        String parameter = "5";
        int actual = marsRoverCommonsUtil.getIntegerValue(parameter);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = MarsRoverException.class)
    public void shouldThrowExceptionWhenGettingIntegerValue() {
        marsRoverCommonsUtil.getIntegerValue("A");
    }

}