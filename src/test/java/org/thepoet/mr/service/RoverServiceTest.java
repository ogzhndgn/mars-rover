package org.thepoet.mr.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.thepoet.mr.enums.Command;
import org.thepoet.mr.enums.Direction;
import org.thepoet.mr.exception.MarsRoverException;
import org.thepoet.mr.model.Location;
import org.thepoet.mr.model.Plateau;
import org.thepoet.mr.model.Rover;
import org.thepoet.mr.service.factory.CommandServiceFactory;
import org.thepoet.mr.service.impl.MoveService;
import org.thepoet.mr.service.impl.TurnLeftService;
import org.thepoet.mr.service.impl.TurnRightService;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RoverServiceTest {

    @Mock
    MarsRoverCommonsUtil marsRoverCommonsUtil;
    @Mock
    CommandServiceFactory factory;
    @InjectMocks
    RoverService roverService;
    private Plateau plateau;
    private Rover rover;

    @Before
    public void init() {
        plateau = this.createPlateau();
        rover = this.createRover();
    }

    @Test
    public void shouldCreateRover() {
        String input = "1 2 W";
        when(marsRoverCommonsUtil.getIntegerValue("1")).thenReturn(1);
        when(marsRoverCommonsUtil.getIntegerValue("2")).thenReturn(2);
        Rover actualRover = roverService.createRoverFromInputAndLocate(input, plateau);
        Assert.assertEquals(Direction.W, actualRover.getDirection());
        Assert.assertEquals(1, actualRover.getLocation().getCoordinateX());
        Assert.assertEquals(2, actualRover.getLocation().getCoordinateY());
        Assert.assertEquals(plateau.getUpperX(), actualRover.getPlateau().getUpperX());
        Assert.assertEquals(plateau.getUpperY(), actualRover.getPlateau().getUpperY());
    }

    @Test(expected = MarsRoverException.class)
    public void shouldThrowExceptionWhenLocationIsOutOfPlateau() {
        String input = "7 8 W";
        when(marsRoverCommonsUtil.getIntegerValue("7")).thenReturn(7);
        when(marsRoverCommonsUtil.getIntegerValue("8")).thenReturn(8);
        roverService.createRoverFromInputAndLocate(input, plateau);
    }

    @Test(expected = MarsRoverException.class)
    public void shouldThrowExceptionWhenLocationIsOccupied() {
        String input = "3 4 W";
        when(marsRoverCommonsUtil.getIntegerValue("3")).thenReturn(3);
        when(marsRoverCommonsUtil.getIntegerValue("4")).thenReturn(4);
        roverService.createRoverFromInputAndLocate(input, plateau);
    }

    @Test
    public void shouldDoCommands() {
        String commands = "RMMLMLMM";
        when(factory.get(Command.L)).thenReturn(new TurnLeftService());
        when(factory.get(Command.R)).thenReturn(new TurnRightService());
        when(factory.get(Command.M)).thenReturn(new MoveService());
        roverService.doCommands(rover, commands);
        Assert.assertEquals(3, rover.getLocation().getCoordinateX());
        Assert.assertEquals(5, rover.getLocation().getCoordinateY());
        Assert.assertEquals(Direction.W, rover.getDirection());
    }

    private Plateau createPlateau() {
        Plateau plateau = new Plateau(6, 6);
        Location location = new Location(3, 4);
        Rover rover = new Rover(location, Direction.S, plateau);
        plateau.addRoverToList(rover);
        return plateau;
    }

    private Rover createRover() {
        Plateau plateau = new Plateau(6, 6);
        Location location = new Location(3, 4);
        return new Rover(location, Direction.N, plateau);
    }
}