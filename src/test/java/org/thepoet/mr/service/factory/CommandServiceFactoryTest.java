package org.thepoet.mr.service.factory;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.thepoet.mr.enums.Command;
import org.thepoet.mr.service.impl.MoveService;
import org.thepoet.mr.service.impl.TurnLeftService;
import org.thepoet.mr.service.impl.TurnRightService;
import org.thepoet.mr.service.spec.CommandService;

@RunWith(MockitoJUnitRunner.class)
public class CommandServiceFactoryTest {
    @InjectMocks
    CommandServiceFactory factory;

    @Test
    public void shouldReturnMoveServiceWhenCommandIsM() {
        CommandService expectedCommand = new MoveService();
        CommandService actualCommand = factory.get(Command.M);
        Assert.assertEquals(actualCommand.getClass().getName(), expectedCommand.getClass().getName());
    }

    @Test
    public void shouldReturnMoveServiceWhenCommandIsL() {
        CommandService expectedCommand = new TurnLeftService();
        CommandService actualCommand = factory.get(Command.L);
        Assert.assertEquals(actualCommand.getClass().getName(), expectedCommand.getClass().getName());
    }

    @Test
    public void shouldReturnMoveServiceWhenCommandIsR() {
        CommandService expectedCommand = new TurnRightService();
        CommandService actualCommand = factory.get(Command.R);
        Assert.assertEquals(actualCommand.getClass().getName(), expectedCommand.getClass().getName());
    }
}