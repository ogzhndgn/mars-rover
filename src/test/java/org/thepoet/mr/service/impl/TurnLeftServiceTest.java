package org.thepoet.mr.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.thepoet.mr.enums.Direction;
import org.thepoet.mr.model.Location;
import org.thepoet.mr.model.Plateau;
import org.thepoet.mr.model.Rover;

@RunWith(MockitoJUnitRunner.class)
public class TurnLeftServiceTest {
    @InjectMocks
    TurnLeftService turnLeftService;
    private Rover rover;

    @Before
    public void init() {
        rover = this.createTestRover();
    }

    @Test
    public void shouldTurnLeftWhenDirectionIsE() {
        rover.setDirection(Direction.E);
        turnLeftService.doCommand(rover);
        Assert.assertEquals(Direction.N, rover.getDirection());
    }

    @Test
    public void shouldTurnLeftWhenDirectionIsN() {
        rover.setDirection(Direction.N);
        turnLeftService.doCommand(rover);
        Assert.assertEquals(Direction.W, rover.getDirection());
    }

    @Test
    public void shouldTurnLeftWhenDirectionIsW() {
        rover.setDirection(Direction.W);
        turnLeftService.doCommand(rover);
        Assert.assertEquals(Direction.S, rover.getDirection());
    }

    @Test
    public void shouldTurnLeftWhenDirectionIsS() {
        rover.setDirection(Direction.S);
        turnLeftService.doCommand(rover);
        Assert.assertEquals(Direction.E, rover.getDirection());
    }

    private Rover createTestRover() {
        Plateau plateau = new Plateau(6, 6);
        Location location = new Location(2, 2);
        return new Rover(location, Direction.N, plateau);
    }
}
