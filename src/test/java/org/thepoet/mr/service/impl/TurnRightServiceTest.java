package org.thepoet.mr.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.thepoet.mr.enums.Direction;
import org.thepoet.mr.model.Location;
import org.thepoet.mr.model.Plateau;
import org.thepoet.mr.model.Rover;

@RunWith(MockitoJUnitRunner.class)
public class TurnRightServiceTest {
    @InjectMocks
    TurnRightService turnRightService;
    private Rover rover;

    @Before
    public void init() {
        rover = this.createTestRover();
    }

    @Test
    public void shouldTurnRightWhenDirectionIsN() {
        rover.setDirection(Direction.N);
        turnRightService.doCommand(rover);
        Assert.assertEquals(Direction.E, rover.getDirection());
    }

    @Test
    public void shouldTurnRightWhenDirectionIsE() {
        rover.setDirection(Direction.E);
        turnRightService.doCommand(rover);
        Assert.assertEquals(Direction.S, rover.getDirection());
    }

    @Test
    public void shouldTurnRightWhenDirectionIsS() {
        rover.setDirection(Direction.S);
        turnRightService.doCommand(rover);
        Assert.assertEquals(Direction.W, rover.getDirection());
    }

    @Test
    public void shouldTurnRightWhenDirectionIsW() {
        rover.setDirection(Direction.W);
        turnRightService.doCommand(rover);
        Assert.assertEquals(Direction.N, rover.getDirection());
    }

    private Rover createTestRover() {
        Plateau plateau = new Plateau(6, 6);
        Location location = new Location(2, 2);
        return new Rover(location, Direction.N, plateau);
    }
}
