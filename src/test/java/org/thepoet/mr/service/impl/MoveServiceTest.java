package org.thepoet.mr.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.thepoet.mr.enums.Direction;
import org.thepoet.mr.exception.MarsRoverException;
import org.thepoet.mr.model.Location;
import org.thepoet.mr.model.Plateau;
import org.thepoet.mr.model.Rover;

@RunWith(MockitoJUnitRunner.class)
public class MoveServiceTest {

    @InjectMocks
    MoveService moveService;
    private Rover rover;


    @Before
    public void init() {
        rover = this.createTestRover();
    }

    @Test
    public void shouldMoveWhenDirectionIsN() {
        int expectedX = rover.getLocation().getCoordinateX();
        int expectedY = rover.getLocation().getCoordinateY() + 1;
        moveService.doCommand(rover);
        Assert.assertEquals(expectedY, rover.getLocation().getCoordinateY());
        Assert.assertEquals(expectedX, rover.getLocation().getCoordinateX());
    }

    @Test
    public void shouldMoveWhenDirectionIsE() {
        rover.setDirection(Direction.E);
        int expectedX = rover.getLocation().getCoordinateX() + 1;
        int expectedY = rover.getLocation().getCoordinateY();
        moveService.doCommand(rover);
        Assert.assertEquals(expectedX, rover.getLocation().getCoordinateX());
        Assert.assertEquals(expectedY, rover.getLocation().getCoordinateY());
    }

    @Test
    public void shouldMoveWhenDirectionIsS() {
        rover.setDirection(Direction.S);
        int expectedX = rover.getLocation().getCoordinateX();
        int expectedY = rover.getLocation().getCoordinateY() - 1;
        moveService.doCommand(rover);
        Assert.assertEquals(expectedX, rover.getLocation().getCoordinateX());
        Assert.assertEquals(expectedY, rover.getLocation().getCoordinateY());
    }

    @Test
    public void shouldMoveWhenDirectionIsW() {
        rover.setDirection(Direction.W);
        int expectedX = rover.getLocation().getCoordinateX() - 1;
        int expectedY = rover.getLocation().getCoordinateY();
        moveService.doCommand(rover);
        Assert.assertEquals(expectedX, rover.getLocation().getCoordinateX());
        Assert.assertEquals(expectedY, rover.getLocation().getCoordinateY());
    }

    @Test(expected = MarsRoverException.class)
    public void shouldThrowExceptionWhenOutOfPlateau() {
        rover.setLocation(new Location(6, 6));
        moveService.doCommand(rover);
    }

    @Test(expected = MarsRoverException.class)
    public void shouldThrowExceptionWhenPointIsOccupied() {
        rover.setLocation(new Location(2, 1));
        rover.getPlateau().addRoverToList(this.createTestRover());
        moveService.doCommand(rover);
    }

    private Rover createTestRover() {
        Plateau plateau = new Plateau(6, 6);
        Location location = new Location(2, 2);
        return new Rover(location, Direction.N, plateau);
    }
}