package org.thepoet.mr.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.thepoet.mr.model.Plateau;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlateauServiceTest {
    @Mock
    MarsRoverCommonsUtil marsRoverCommonsUtil;

    @InjectMocks
    PlateauService plateauService;

    @Test
    public void shouldCreatePlateauFromInput() {
        String input = "5 6";
        when(marsRoverCommonsUtil.getIntegerValue("5")).thenReturn(5);
        when(marsRoverCommonsUtil.getIntegerValue("6")).thenReturn(6);
        Plateau actualPlateau = plateauService.createPlateauFromInput(input);
        Assert.assertEquals(5, actualPlateau.getUpperX());
        Assert.assertEquals(6, actualPlateau.getUpperY());
    }
}